# The Dog Quarantine Predictor #

### Purpose ###

Whenever a dog is involved in a biting incident, an investigation will be opened, and the dog could be quarantined.
To help with this decision, the dog quarantine predictor was made. The app utilizes machine learning to predict whether
the dog in question is dangerous for rabies and should be quarantined or if the dog is harmless.

Version 1.0.0

### Set up ###

The application requires a java version of 11 or higher to be run, alongside some dependencies:

- [Apache CLI](https://commons.apache.org/proper/commons-cli/) version 1.4 is used to parse the commandline
 and provide the user with help if requested.
 
- [The Weka API](https://waikato.github.io/weka-wiki/use_weka_in_your_java_code/) version 3.8.0 
 is used to classify the instances and write them to an output file

- [JUnit5](https://junit.org/junit5/docs/current/user-guide/) version 5.6.2 is used for testing within the code.

These dependencies need to be installed within the build.gradle file and can be done as follows.
(If the build.gradle from this repository is used, the packages will automatically be installed when loading the file.)

```
dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
    compile('commons-cli:commons-cli:1.4')
    compile group: 'nz.ac.waikato.cms.weka', name: 'weka-stable', version: '3.8.0'
}
```

The rest of the build information can also be found within the build.gradle file.

### How to Run ###

The application can be found within the build/libs folder as a .jar file. Because of this, the application works on
every system with java installed.

The application can run in two different ways. It works with file input and single input, which will be described first.
If more information is required, the '--help' command can be utilized.

```
$ java -jar DogQuarantinePredictor-1.0.0-all.jar --help
``` 

##### Single Input #####

If you want to classify a single instance provided via the command line, the correct type option needs to be set, 
and parameters need to be submitted. The following line of code shows how to run this option within the terminal.

```
$ java -jar DogQuarantinePredictor-1.0.0-all.jar -t single -b <Dog Breed> -g <Gender> -c <Color> -v <Vaccination Years> -w <Bite Location>
```

if the dog breed consists of two words like 'pitt bull', the breed should be written with quotation marks like so:

```
$ -b <"Pitt Bull">
```

##### File Input

If a file containing all the instances should be submitted, the type needs to be set to 'file', and the correct file path
should be submitted. The following line of code shows how to run this option within the terminal.

```
$ java -jar DogQuarantinePredictor-1.0.0-all.jar -t file -f <path to file>
``` 

An import thing to note is the content of the file. The file should be in the CSV format (comma separated) and contain the specific 
columns as shown in the following example.

|Breed,Gender,Color,Vaccination Years,Where,Vaccinated|
|-----------------------------------------------------|
|DACHSHUND,MALE,Red,1,BODY,Yes                        |
|SHIH TZU,MALE,White,3,BODY,Yes                       |
|...                                                  |                      

#### Saving Output ####

Running the examples above does not save the results, but prints these to the terminal. if the results should be saved,
an extra parameter should be added specifying the output type (.csv / .arff) at the end.

```
$ java -jar DogQuarantinePredictor-1.0.0-all.jar -t single -b <Dog Breed> -g <Gender> -c <Color> -v <Vaccination Years> -w <Bite Location> -o <csv / arff>

$ java -jar DogQuarantinePredictor-1.0.0-all.jar -t file -f <path to file> -o <csv / arff>
```

### Contact ###

If you have questions or any issues, feel free to contact me at:
ja.m.van.eijk@st.hanze.nl

### Extra ###

The information/theory behind this application can be found in [This Repo](https://bitbucket.org/Jmvaneijk/thema_9/src/master/).