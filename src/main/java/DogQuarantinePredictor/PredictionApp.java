package DogQuarantinePredictor;

/**
 * Main class collecting the commandline arguments and passing them to be parsed by ProcessCLI
 * Using Apache CLI, this will collect the instances, classify them and save or print the results.
 *
 * @author Jamie van Eijk
 */
public class PredictionApp {

    /**
     * @param args The command line arguments
     */
    public static void main(String[] args) {
            ProcessCLI processor = new ProcessCLI(args);
            processor.start();
    }
}