package DogQuarantinePredictor;

import org.apache.commons.cli.*;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * This class uses the OptionsProvider interface to parse the command line arguments.
 * These parsed arguments will be used to either collect the instances from a file
 * or create a new instance, which will be classified and saved to a file or printed
 * depending on user input.
 *
 * @author Jamie van Eijk
 */
public class ProcessCLI {
    // Creating final strings that are used for the options
    private static final String HELP = "help";
    private static final String TYPE = "type";
    private static final String FILE = "file";
    private static final String BREED = "breed";
    private static final String GENDER = "gender";
    private static final String COLOR = "color";
    private static final String VACCINATIONYRS = "vaccinationyrs";
    private static final String WHERE = "where";
    private static final String OUTPUT = "output";

    // Saving some important variables within "this"
    private final String[] arguments;
    private Options options;
    private CommandLine cmd;

    /**
     * Constructor using the command line array.
     *
     * @param args the command line array
     */
    public ProcessCLI(final String[] args) {
        this.arguments = args;
    }

    /**
     * Public start function calls all the important methods.
     */
    public void start() {
        createOptions();
        parseCMD();
        Instances unclassified = getInstances();
        Instances classified = classifyInstances(unclassified);
        writeInstances(classified);
    }

    /**
     * All the different options are added to the Options Object.
     */
    private void createOptions() {
        // Creating all the options
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Shows the user the help");
        Option typeOption = new Option("t", TYPE, true, "Input type; "
                + "If you want to submit a single instance, choose 'single' "
                + "In case you would like to use a CSV file, choose 'file' ");
        Option fileOption = new Option("f", FILE, true, "The filename/location path");
        Option breedOption = new Option("b", BREED, true, "The breed of the dog (e.g. Pit bull) "+
                "Please use \"\" around the breed if it contains spaces.");
        Option genderOption = new Option("g", GENDER, true, "The gender of the dog (Male/Female)");
        Option colorOption = new Option("c", COLOR, true, "The color of the dog (e.g. Black)");
        Option vaccinationyrsOption = new Option("v", VACCINATIONYRS, true,
                "The amount of years the dog was vaccinated, if the dog was not vaccinated, please use 0");
        Option whereOption = new Option("w", WHERE, true,
                "Where was the victim bitten? (Head/Body)");
        Option outputOption = new Option("o", OUTPUT, true,
                "In what format should je results be saved? .cvs or .arff? " +
                        " If the results should be printed, please do not include this option! ");

        // Adding all the options to the options object
        options.addOption(helpOption);
        options.addOption(typeOption);
        options.addOption(fileOption);
        options.addOption(breedOption);
        options.addOption(genderOption);
        options.addOption(colorOption);
        options.addOption(vaccinationyrsOption);
        options.addOption(whereOption);
        options.addOption(outputOption);

    }

    /**
     * Parses the command line and prints the help menu to the user if requested.
     */
    private void parseCMD(){
        CommandLineParser parser = new DefaultParser();
        HelpFormatter hf = new HelpFormatter();
        hf.setWidth(125);
        try{
            this.cmd = parser.parse(this.options, this.arguments);
            if (cmd.hasOption(HELP)) {
                hf.printHelp("Dog Quarantine Predictor",this.options, true);
                System.exit(0);
            }
        } catch (ParseException e) {
            System.err.println("Incorrect or missing option provided, please try again!");
            hf.printHelp("Dog Quarantine Predictor", this.options, true);
            System.exit(0);
        }
    }

    /**
     * If the user requested file or single input, the corresponding functions will be called to get the instances
     * from a file or single CL input.
     * When a wrong option, or no option at all is provided, the code shuts down.
     *
     * @return The unclassified instances from either a CSV file or CL input
     */
    private Instances getInstances() {
        if (cmd.hasOption(TYPE)) {
            String type = cmd.getOptionValue(TYPE).trim();
            type = type.toLowerCase();
            switch (type) {
                case "single":
                    return singleToInstance();
                case "file":
                    return CSVToInstances();
                default:
                    throw new IllegalArgumentException("Type: " + type + " Is not correct, please use" +
                            " 'single' or 'file' and check --help. Shutting down...");

            }
        } else {
            System.err.println("No Type option was submitted, Please use --help to see the parameters.");
            System.exit(0);
            return null;
        }
    }

    /**
     * If the requested input type was a file, this method check is the file is valid
     * and collects the instances from the file.
     *
     * @return The unclassified instances from the CSV-file
     */
    private Instances CSVToInstances() {
        if (cmd.hasOption(FILE)) {
            String filePath = cmd.getOptionValue(FILE);

            try {
                final InputStream file = new FileInputStream(filePath);

                CSVLoader csvLoader = new CSVLoader();
                csvLoader.setSource(file);

                Instances instances = csvLoader.getDataSet();

                instances = setClass(instances);
                return instances;
            } catch (IOException e) {
                System.err.println("File: " + filePath + " Not found or unable to load, please try a valid file " +
                        "as specified in the README");
                System.exit(0);
                return null;
            }

        } else {
            System.err.println("No file was submitted, shutting down...");
            System.exit(0);
            return null;
        }
    }

    /**
     * If the requested input type was single, this method checks all the provided CL info and
     * creates a new instance.
     *
     * @return The unclassified instances from CL input
     */
    private Instances singleToInstance() {
        if (cmd.hasOption(BREED) && cmd.hasOption(GENDER) && cmd.hasOption(COLOR)
                && cmd.hasOption(VACCINATIONYRS) && cmd.hasOption(WHERE)) {

            String breed = cmd.getOptionValue(BREED);
            String gender = cmd.getOptionValue(GENDER);
            String color = cmd.getOptionValue(COLOR);
            String vaccinationYrs = cmd.getOptionValue(VACCINATIONYRS);
            String whereBitten = cmd.getOptionValue(WHERE);
            String vaccinated = getVaccinatedStatus(vaccinationYrs);

            validateInput(gender, vaccinationYrs, whereBitten);

            ArrayList<Attribute> attributes = new ArrayList<>(5);

            attributes.add(new Attribute("Breed",(ArrayList<String>)null));
            attributes.add(new Attribute("Gender",(ArrayList<String>)null));
            attributes.add(new Attribute("Color",(ArrayList<String>)null));
            attributes.add(new Attribute("Vaccination Years",(ArrayList<String>)null));
            attributes.add(new Attribute("Where Bitten",(ArrayList<String>)null));
            attributes.add(new Attribute("Vaccinated",(ArrayList<String>)null));

            Instances instances = new Instances("Dog Quarantine Test", attributes,0);

            double[] dogInstance = new double[instances.numAttributes()];
            dogInstance[0] = instances.attribute(0).addStringValue(breed);
            dogInstance[1] = instances.attribute(1).addStringValue(gender);
            dogInstance[2] = instances.attribute(2).addStringValue(color);
            dogInstance[3] = instances.attribute(3).addStringValue(vaccinationYrs);
            dogInstance[4] = instances.attribute(4).addStringValue(whereBitten);
            dogInstance[5] = instances.attribute(5).addStringValue(vaccinated);

            instances.add(new DenseInstance(1.0, dogInstance));

            instances = setClass(instances);
            return instances; }
        else {
            System.err.println("Some required fields are missing arguments," +
                    " please check --help and fill in all the missing fields!");
            System.exit(0);
            return  null;
        }
    }

    /**
     * Some of the submitted input is checked for correct information.
     * If the information is incorrect, a new error will be thrown, otherwise nothing will happen.
     *
     * @param gender The gender of the dog submitted by the user
     * @param vaccinationYrs The amount of years the dog has been vaccinated, submitted by the user
     * @param whereBitten The location where the victim was bitten, submitted by the user
     */
    private void validateInput(String gender, String vaccinationYrs, String whereBitten) {

        // Checks if a correct gender has been submitted.
        if (!gender.toLowerCase().matches("male|female")) {
            throw new IllegalArgumentException("Gender has to be Male or Female, " +
                    gender + " is not a valid gender! please try again!");
        }

        // Checks if a correct amount of vaccination years have been submitted.
        if (Integer.parseInt(vaccinationYrs) < 0) {
            throw new IllegalArgumentException("A dog cannot be vaccinated for a negative amount of " +
                    "years, please try again!");
        }

        // Check if a correct location has been submitted.
        if (!whereBitten.toLowerCase().matches("head|body")) {
            throw new IllegalArgumentException("The " + whereBitten + " is not a valid location for a bite," +
                    "please specify between head or body.");
        }
    }

    /**
     * Since the created Instance objects are missing their class attribute, this methods adds those.
     *
     * @param instances Original instance object missing the class attribute.
     *
     * @return The Instances object with class attribute added
     */
    private Instances setClass(Instances instances){
        Instances unclassified = new Instances(instances);

        Add filter = new Add();
        filter.setAttributeIndex("last");
        filter.setNominalLabels("yes,no");
        filter.setAttributeName("Quarantined");
        try {
        filter.setInputFormat(unclassified);
        unclassified = Filter.useFilter(unclassified, filter);

        unclassified.setClassIndex(unclassified.numAttributes() - 1);
        return unclassified;
        } catch (Exception e) {
            System.err.println("Filter could not be applied, please try again!");
            System.exit(0);
        }
    return null;
    }

    /**
     * Using the vaccination years information, this method decides whether the dog was vaccinated or not.
     *
     * @param yrs the amount of years the dog was vaccinated
     *
     * @return The vaccination status
     */
    private String getVaccinatedStatus(String yrs) {
        String vaccinated;
        if (Integer.parseInt(yrs) == 0) {
            vaccinated = "no";
        } else {
            vaccinated = "yes";
        }
        return vaccinated;
    }

    /**
     * The unclassified instances are subjected to the ClassifyInstances class and classified.
     *
     * @param unclassifiedInstances Instances object with empty class attributes which need to be filled
     *
     * @return The classified instances
     */
    private Instances classifyInstances(Instances unclassifiedInstances) {
        ClassifyInstances classifier = new ClassifyInstances(unclassifiedInstances);
        return classifier.classify();
    }

    /**
     * Based on the output type information from the CL, the classified instances are send to the
     * WriteToFile class and written to their corresponding file or printed to the CL.
     *
     * @param classifiedInstances Instance object with classified instances
     */
    private void writeInstances(Instances classifiedInstances){
        if (cmd.hasOption(OUTPUT)){
            String outputType = cmd.getOptionValue(OUTPUT);
            WriteToFile writer = new WriteToFile(classifiedInstances, outputType);
            writer.writeOutput();
        } else {
            System.out.println("No output option was provided, printing the results!");
            System.out.println(classifiedInstances);
            System.exit(0);
        }
    }
}
