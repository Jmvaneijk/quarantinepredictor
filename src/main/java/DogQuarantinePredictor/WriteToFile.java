package DogQuarantinePredictor;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;

import java.io.File;
import java.io.IOException;

/**
 * This class utilizes the Weka API to save the classified instances to the requested output file.
 *
 * @author Jamie van Eijk
 */
public class WriteToFile {
    private final Instances classifiedInstances;
    private final String outputType;

    /**
     * constructor getting the classified instances and output type.
     *
     * @param classifiedInstances The classified instances that will be written to a file
     * @param outputType The requested file format to which the results should be written
     */
    public WriteToFile(Instances classifiedInstances, String outputType) {
        this.classifiedInstances = classifiedInstances;
        this.outputType = outputType.toLowerCase();
    }

    /**
     * Using a switch/case, the writer method corresponding to the type will be utilized
     */
    public void writeOutput() {
        switch(outputType){
            case "csv":
                writeToCSV();
                break;
            case  "arff":
                writeToARFF();
                break;
            default:
                System.err.println("Type: " + outputType + " is not a valid file format, please check --help");
                System.exit(0);
        }
    }

    /**
     * If the user requested .arff output, the instances will be written to an .arff file.
     */
    private void writeToARFF(){
        ArffSaver saver = new ArffSaver();
        saver.setInstances(classifiedInstances);
        try {
            saver.setFile(new File("data/ClassifiedResults.arff"));
            saver.writeBatch();
        } catch (IOException e) {
            System.err.println("Something went wrong with creating the output file, please try again!");
            e.printStackTrace();
        }
    }

    /**
     * If the user requested .csv output, the instances will be written to a .csv file.
     */
    private void writeToCSV(){
        CSVSaver saver = new CSVSaver();
        saver.setInstances(classifiedInstances);
        try {
            saver.setFile(new File("data/ClassifiedResults.csv"));
            saver.writeBatch();
        } catch (IOException e) {
            System.err.println("Something went wrong with creating the output file, please try again!");
            e.printStackTrace();
        }
    }
}


