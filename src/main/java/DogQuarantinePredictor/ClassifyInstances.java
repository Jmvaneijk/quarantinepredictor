package DogQuarantinePredictor;

import weka.classifiers.meta.Bagging;
import weka.core.Instances;

/**
 * This class utilizes the Weka API to load the model and classify the instances that were supplied.
 *
 * @author Jamie van Eijk
 */
public class ClassifyInstances {
    private final Instances instances;

    /**
     * constructor getting the unclassified instances.
     *
     * @param instances The unclassified instances extracted from a file or created from CL input
     */
    public ClassifyInstances(Instances instances) {
        this.instances = instances;
    }

    /**
     * Classify each instance and update the class attribute with the result.
     *
     * @return The classified instances
     */
    public Instances classify() {
        Bagging bagging_rf = loadModel();

        try {
        for (int i = 0; i < instances.numInstances(); i++) {
            double label = bagging_rf.classifyInstance(instances.instance(i));
            instances.instance(i).setClassValue(label);
        }}
        catch (Exception e) {
            System.err.println("Unable to classify instances, please submit a file with the information " +
                    "as specified in the README");
            System.exit(0);
        }
        return instances;
    }

    /**
     * Loads the model that was created using the Weka Gooey.
     *
     * @return The loaded Bagging model
     */
    private Bagging loadModel() {
        try { return (Bagging)weka.core.SerializationHelper.read("data/Bagging_RandomForest.model"); }
        catch (Exception e) {
            System.err.println("Unable to Load model, please try again!");
            System.exit(0);
        }
        return null;
    }
}
